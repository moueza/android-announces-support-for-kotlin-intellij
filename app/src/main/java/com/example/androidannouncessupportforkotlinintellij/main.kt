package helloWorld

import android.util.Size
import kotlin.Array as Array1



data class Customer(var name: String, var email: String = "",
                    var company: String = "") {


    //nullable
    fun nullable() {
        var neverNull: String = "something"
        var mightBeNull: String? = null // "?" indicates this can be null

        if (neverNull.length > 0) {
            // This is OK
            //  …
        }

        if (mightBeNull.length > 0) { // Compiler catches this error for you
            // …
        }
    }



   // Named parameters and default arguments
    fun orderPizza(size: Size,
                   pepperoni: Boolean = false,
                   mushrooms: Boolean = false,
                   ham: Boolean = false,
                   pineapple: Boolean = false,
                   pickles: Boolean = false,
                   sausage: Boolean = false,
                   peppers: Boolean = false,
                   onion: Boolean = false)
    {
        //...
    }









    fun main(args: Array1) {
        println("Hello World!")


        orderPizza(Size.LARGE, ham = true, mushrooms = true);




        /** When statement
        Kotlin has a variation of a switch statement that allows matching on arbitrary expressions.*/
// Please don't put this in your app!
        when {
            password.equals("password") -> println("Insecure password!")
            password.length < 4 -> println("Too short!")
            else -> {
                println("Secure password!")
            }
        }
    }


}



